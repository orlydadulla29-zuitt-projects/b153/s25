//import Node's built-in HTTP module, a set of tools that can handle server requests and responses, etc.

let http = require("http")

/*const requestListener = (request, response) => {
	response.writeHead(200, {'Content-Type':'text/plain'})

	response.end("Hello Orly!")
} */

const server = http.createServer(function(request, response){
	response.writeHead(200, {'Content-Type':'text/plain'})
	response.end("Hello Orly!")
})

const port = 4000

server.listen(port)

console.log(`Server running at port ${port}`)